DISTRIBUTED FILE SYSTEM - Oliver Cormerais - 10703297

System Components:
- Files1 (directory)
- Files2 (directory)
- Files3 (directory)
- DirectoryServer.rb
- FileServer1.rb
- FileServer2.rb
- FileServer3.rb
- LockServer.rb
- Client.rb

The set of features that were implemented are:
- Distributed Transparent File Access
- Directory Service
- Replication
- Lock Service

The basic system uses the Upload/Download model.

To test the system, spin up all servers and the required amount of clients.  Clients are prompted as to which file they would like to access (e.g. abc.txt). The request is then sent to the directory server who makes use of the Lock Server to determine whether the file is locked or unlocked. If the file is unlocked, the port of the relevent file server is sent back to the client. the client then connects to the file server containing the file and downloads the file into an automatically created local directory (called Client1 for example).
Modifications can then be made to the file  and when all changes have been done, the client is prompted to upload file to the file server at which the stage the local directory is deleted. If the file is replicated on other servers, then the versions of these files are updated. The replication model used is therefore similar to a Primary copy model. Should other clients require access to a file already being used, it downloads them from the other servers but in read only mode (cannot upload changes).