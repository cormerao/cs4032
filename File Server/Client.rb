#Name: Oliver Cormerais
#Student No: 10703297
#CS4032: File Server Client

#!/usr/bin/ruby

require 'socket'      # Sockets are in standard library
require 'fileutils'

hostname = 'localhost'
port = 8000

s = TCPSocket.open(hostname, port)

def create_tree(directories, parent='.')
  directories.each_pair do |dir, files|
    path = File.join(parent,dir)
    Dir.mkdir path unless File.exists? path
    files.each do |filename,contents|
      if filename.respond_to? :each_pair    # It's a subdirectory
        create_tree filename, path
      else                                  # It's a file
        open(File.join(path,filename), 'w') {|f| f << contents || ""}
      end
    end
  end
end


def setup(s)
  print 'Please enter required file: \'abc.txt\', \'file_b.txt\', \'file_c.txt\', \'bla.txt\' or \'tad.txt\': '
  file = gets.chomp
  s.puts(file)
  response = s.gets.chomp
  puts response
  if  response.include?('PORT: ')
    client_number = s.gets.chomp
    read_write = s.gets.chomp
    port = response.split(' ')[1]
    s = TCPSocket.open('localhost', port)
    download(read_write, s, file, client_number)
    if read_write == 'write'
      print "Enter 1 to upload document to server: "
      while (gets.chomp != '1')
      end
      upload(port, s, file, client_number)
    else
      print "READ ONLY MODE\nEnter 1 to close document: "
      while (gets.chomp != '1')
      end
      FileUtils.rm_rf("Client#{client_number}")
    end

  end

end

def upload(port, s, file_name, client_number)
  puts "Uploaded file #{file_name} to server #{Socket.gethostname}..."
  file = File.open("Client#{client_number}/#{file_name}", 'rb')
  fileContent = file.read
  fileContent += "\nendFlag"
  s.puts(fileContent)
  s_lock = TCPSocket.open('localhost', 8001)
  s_lock.puts(file_name)
  s_lock.puts(port)
  s_lock.puts('unlock')
  file.close
  FileUtils.rm_rf("Client#{client_number}")
end

def download(read_write, s, file_name, client_number)
  s.puts('update')
  s.puts(file_name)
  s.puts(read_write)
  file_data = ''
  while (file_line = s.gets.chomp) != 'endFlag'
    file_data << file_line
    file_data << "\n"
  end
  create_tree "Client#{client_number}" => []
  destFile = File.open("Client#{client_number}/#{file_name}", 'wb');
  destFile.print file_data
  destFile.close
  puts 'File downloaded.'
end

loop {
  print "\nEnter 1 to access files, enter 2 to KILL_SERVICE, enter 3 for HELO message, enter 4 to DISCONNECT: "
  choice = gets.chomp
  if choice == '1'
    setup(s)
  elsif choice == '2'
    s.puts('KILL_SERVICE')
    break
  elsif choice == '3'
    print 'Enter message: '
    helo_message = gets.chomp
    s.puts("HELO #{helo_message}")
    4.times do
      line = s.gets   # Read response from the server
      puts line      # print out response
    end
  elsif choice == '4'
    s.puts('DISCONNECT: ')
    break
  end
}