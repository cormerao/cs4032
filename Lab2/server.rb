#Name: Oliver Cormerais
#Student No: 10703297
#CS4032: Lab 2

#!/usr/bin/ruby

require 'socket'                # Get sockets from library
require 'thread'

$DEBUG = 1
port = 8000
max_clients = 5

# ThreadPool class
class ThreadPool
  def initialize(max_clients)
    @pool = []
    @max_clients = max_clients
    @pool_semaphore = Mutex.new
    @pool_cv = ConditionVariable.new
  end

  def dispatch(*args)
    Thread.new do
      @pool_semaphore.synchronize do
        while @pool.size >= @max_clients
          print "Pool is full; waiting to run #{args.join(', ')}...\n" if $DEBUG
          # wait until some other thread calls @pool_cv.signal
          @pool_cv.wait(@pool_semaphore)
        end
      end
      @pool << Thread.current   # add thread to pool
      print "Current number of active threads: #{@pool.size}\n"
      # exception block starts here
      begin
        yield(*args)
      rescue => e
        exception(self, e, *args)
      ensure
        @pool_semaphore.synchronize do
          @pool.delete(Thread.current)  # delete current thread
          print "Current number of active threads: #{@pool.size}\n\n"
          @pool_cv.signal       # signal to pool that there is a vacancy in the pool
        end
      end #exception block ends here
    end
  end

  def exception(thread, exception, *original_args)
    puts "Exception in thread #{thread}: #{exception}"
  end

end
# main code starts here
server = TCPServer.open(port)   # Socket to listen on port 8000
pool = ThreadPool.new(max_clients)  # declare pool with max number of clients

loop {                          # Servers run forever
  Thread.start(server.accept) do |client|
    pool.dispatch(client) do |client|
      while message = client.gets.chomp   # Read lines from the socket
        if message == "KILL_SERVICE"
          exit     # ends server and all clients
        elsif message.include?("HELO")    # if message includes 'HELO' string
          response = "#{message}\nIP: #{Socket.gethostname}\nPort: #{port}\nStudent ID: 10703297"
          client.puts(response)
        else
          response = "Unknown message: " + message
          client.puts(response)
        end
        client.close    # close current client connection
      end
    end
  end
}