#Name: Oliver Cormerais
#Student No: 10703297
#CS4032: Lab 4 CHAT CLIENT

#!/usr/bin/ruby

require 'socket'      # Sockets are in standard library

hostname = 'localhost'
port = 8000
$restart = 0

s = TCPSocket.open(hostname, port)

def setup(s)
  print 'Enter chat room name: '
  room = gets.chomp
  print 'Enter name: '
  name = gets.chomp
  message = "JOIN_CHATROOM: #{room}\nCLIENT_IP: 0\nPORT: 0\nCLIENT_NAME: #{name}\n"
  s.puts(message)
  m = []
  m[0] = s.gets   # Read response from the server
  puts "\n" + m[0]
  m[1] = s.gets   # Read response from the server
  puts m[1]
  m[2] = s.gets   # Read response from the server
  puts m[2]
  m[3] = m[1].split(' ')  #room ref
  m[4] = m[2].split(' ')  #join id

  return m, name
end

def post(m, name, s)
  loop do
    #print "#{name}: "
    mess = gets.chomp
    if $restart == 1
      break
    end
    chat_message = "CHAT: #{m[3][1]}\nJOIN_ID: #{m[4][1]}\nCLIENT_NAME: #{name}\nMESSAGE: #{mess}\n\n"
    s.puts(chat_message)
    break if $restart == 1
  end
end

def listen(s, name)
  while response = s.gets.chomp
    puts "\n" + response
    if response.include?('LEFT_CHATROOM: ')
      response = s.gets.chomp
      puts response
      if response.include?(name)
        $restart = 1
        break
      end
    else
      response = s.gets.chomp
      puts response
      response = s.gets
      puts response
    end
  end
end
loop {
  $restart = 0
  print "\nEnter 1 to join chatroom, enter 2 to KILL_SERVICE, enter 3 for HELO message, enter 4 to DISCONNECT: "
  choice = gets.chomp
  if choice == '1'
    variables = setup(s)
    m = variables[0]
    name = variables[1]
    t1 = Thread.new{post(m, name, s)}
    t2 = Thread.new{listen(s, name)}
    t1.join
    t2.join
  elsif choice == '2'
    s.puts('KILL_SERVICE')
    break
  elsif choice == '3'
    print 'Enter message: '
    helo_message = gets.chomp
    s.puts("HELO #{helo_message}")
    4.times do
      line = s.gets   # Read response from the server
      puts line      # print out response
    end
  elsif choice == '4'
    s.puts('DISCONNECT: ')
    break
  end
}