#Name: Oliver Cormerais
#Student No: 10703297
#CS4032: Lock Server

#!/usr/bin/ruby

require 'socket'                # Get sockets from library
require 'thread'

port = 8001
max_clients = 10

# ThreadPool class
class ThreadPool
  def initialize(max_clients)
    @pool = []
    @max_clients = max_clients
    @pool_semaphore = Mutex.new
    @pool_cv = ConditionVariable.new
  end

  def dispatch(*args)
    Thread.new do
      @pool_semaphore.synchronize do
        while @pool.size >= @max_clients
          print "Pool is full; waiting to run #{args.join(', ')}...\n"
          # wait until some other thread calls @pool_cv.signal
          @pool_cv.wait(@pool_semaphore)
        end
      end
      @pool << Thread.current   # add thread to pool
      print "Current number of active threads: #{@pool.size}\n"
      # exception block starts here
      begin
        yield(*args)
      rescue => e
        exception(self, e, *args)
      ensure
        @pool_semaphore.synchronize do
          @pool.delete(Thread.current)  # delete current thread
          print "Current number of active threads: #{@pool.size}\n\n"
          @pool_cv.signal       # signal to pool that there is a vacancy in the pool
        end
      end #exception block ends here
    end
  end

  def exception(thread, exception, *original_args)
    puts "Exception in thread #{thread}: #{exception}"
  end
end

# main code starts here
server = TCPServer.open(port)   # Socket to listen on port 8001
pool = ThreadPool.new(max_clients)  # declare pool with max number of clients
h = Hash['abc.txt', Hash[1000, 0, 1001, 0, 1002, 0], 'file_b.txt', Hash[1000,0], 'file_c.txt', Hash[1000,0], 'bla.txt', Hash[1001,0], 'tad.txt', Hash[1002,0]];
count = 0

puts "Lock Server (port: 8001)"

loop {                          # Servers run forever
  Thread.start(server.accept) do |client|
    pool.dispatch(client) do |c|
      loop {
        file_name = c.gets.chomp
        port = c.gets.chomp
        if c.gets.chomp == 'unlock'   #if client unlocks file
          h[file_name][port] = 0
        else
          file_state = h[file_name][port]
          if file_state == 1
            c.puts("locked")
          else
            c.puts("unlocked")
            h[file_name][port] = 1
          end
        end
        c.close
      }
    end
  end
}