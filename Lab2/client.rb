#Name: Oliver Cormerais
#Student No: 10703297
#CS4032: Lab 2

#!/usr/bin/ruby

require 'socket'      # Sockets are in standard library

hostname = 'localhost'
port = 8000

s = TCPSocket.open(hostname, port)
print 'Enter a message to send the server: '  # ask user for message
message = gets.chomp                     # record input message
s.puts(message)

while line = s.gets   # Read response from the server
  puts line.chop      # print out response
end