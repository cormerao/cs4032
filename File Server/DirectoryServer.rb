#Name: Oliver Cormerais
#Student No: 10703297
#CS4032: Directory Server

#!/usr/bin/ruby

require 'socket'                # Get sockets from library
require 'thread'

port = 8000
max_clients = 10

# ThreadPool class
class ThreadPool
  def initialize(max_clients)
    @pool = []
    @max_clients = max_clients
    @pool_semaphore = Mutex.new
    @pool_cv = ConditionVariable.new
  end

  def size
    return @pool.size
  end

  def dispatch(*args)
    Thread.new do
      @pool_semaphore.synchronize do
        while @pool.size >= @max_clients
          print "Pool is full; waiting to run #{args.join(', ')}...\n"
          # wait until some other thread calls @pool_cv.signal
          @pool_cv.wait(@pool_semaphore)
        end
      end
      @pool << Thread.current   # add thread to pool
      print "Current number of active threads: #{@pool.size}\n"
      # exception block starts here
      begin
        yield(*args)
      rescue => e
        exception(self, e, *args)
      ensure
        @pool_semaphore.synchronize do
          @pool.delete(Thread.current)  # delete current thread
          print "Current number of active threads: #{@pool.size}\n\n"
          @pool_cv.signal       # signal to pool that there is a vacancy in the pool
        end
      end #exception block ends here
    end
  end

  def exception(thread, exception, *original_args)
    puts "Exception in thread #{thread}: #{exception}"
  end
end

# main code starts here
server = TCPServer.open(port)   # Socket to listen on port 8000
pool = ThreadPool.new(max_clients)  # declare pool with max number of clients
h = Hash['abc.txt', [1000,1001,1002], 'file_b.txt', 1000, 'file_c.txt', 1000, 'bla.txt', 1001, 'tad.txt', 1002];
count = 0

puts 'Directory Server (port: 8000)'
puts 'File - Port:'
h.keys.each {|x|
  if h[x].is_a?(Array)
    h[x].each {|y|
      if h[x][y] == 0
        puts "#{x} - #{y}"
      else
        puts "#{x} - #{y}"
      end
    }
  else
    if h[x] == 0
      puts "#{x} - #{h[x]}"
    else
      puts "#{x} - #{h[x]}"
    end
  end
}

def checkLock (message, port)
  puts "Checking if file #{message} is locked on port #{port}..."
  s = TCPSocket.open('localhost', 8001)
  s.puts(message)
  s.puts(port)
  s.puts('')
  answer = s.gets.chomp
  return answer
end

loop {                          # Servers run forever
  Thread.start(server.accept) do |client|
    pool.dispatch(client) do |c|
      count += 1
      client_number = count
      loop {
        message = ""
        while (lines = c.gets.chomp) != ""  # Read lines from the socket
          if lines.include?('DISCONNECT: ')
            option = 4
            message = lines
            puts message
            break
          elsif lines == 'KILL_SERVICE'
            option = 3
            message = lines
            puts message
            break
          elsif lines.include?('HELO ')
            message = lines
            puts message
            option = 2
            break
          else
            option = 1
            message = lines
            puts message
            break
          end
        end
        if option == 3
          exit
        elsif option == 4
          c.close
        elsif option == 2
          response = "#{message}\nIP:#{Socket.gethostname}\nPort:#{port}\nStudentID:10703297"
          c.puts(response)
        elsif option == 1
          port = h[message]
          if (port == nil)
            c.puts("Unknown file.")
          elsif port.is_a?(Array)
            port.each {|p|
              answer = checkLock(message,p)
              if (answer == "locked")
                puts "File #{message} is locked on port #{p}."
                if p == port[port.length-1]
                  c.puts("File #{message} is locked on all ports.")
                end
              else
                puts "File #{message} is unlocked."
                c.puts("PORT: #{p}")
                puts "Sent port number to client #{Socket.gethostname}."
                c.puts("#{client_number}")
                if (p == port[0])
                  c.puts('write')
                else
                  c.puts('read')
                end
                break
              end
            }
          else
            answer = checkLock(message,port)
            if (answer == "locked")
              puts "File #{message} is locked."
              c.puts("File #{message} is locked on port #{port}")
            else
              puts "File #{message} is unlocked."
              c.puts("PORT: #{port}")
              puts "Sent port number to client #{Socket.gethostname}."
              c.puts("#{client_number}")
              c.puts('write')
            end
          end
        end
      }
    end
  end
}