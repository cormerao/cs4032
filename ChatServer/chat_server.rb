#Name: Oliver Cormerais
#Student No: 10703297
#CS4032: Lab 4 CHAT SERVER

#!/usr/bin/ruby

require 'socket'                # Get sockets from library
require 'thread'

port = 8000
max_clients = 10

# ThreadPool class
class ThreadPool
  def initialize(max_clients)
    @pool = []
    @max_clients = max_clients
    @pool_semaphore = Mutex.new
    @pool_cv = ConditionVariable.new
  end

  def dispatch(*args)
    Thread.new do
      @pool_semaphore.synchronize do
        while @pool.size >= @max_clients
          print "Pool is full; waiting to run #{args.join(', ')}...\n"
          # wait until some other thread calls @pool_cv.signal
          @pool_cv.wait(@pool_semaphore)
        end
      end
      @pool << Thread.current   # add thread to pool
      print "Current number of active threads: #{@pool.size}\n"
      # exception block starts here
      begin
        yield(*args)
      rescue => e
        exception(self, e, *args)
      ensure
        @pool_semaphore.synchronize do
          @pool.delete(Thread.current)  # delete current thread
          print "Current number of active threads: #{@pool.size}\n\n"
          @pool_cv.signal       # signal to pool that there is a vacancy in the pool
        end
      end #exception block ends here
    end
  end

  def exception(thread, exception, *original_args)
    puts "Exception in thread #{thread}: #{exception}"
  end
end

#Chatroom Class
class Chatroom
  def initialize(name, ref)
    @name = name
    @clients = []
    @ref = ref
    puts "Chatroom #{ref} created"
  end

  def serve(member, member_name)
    m = []
    @clients << member
    join_id = @clients.length
    puts "Member added: #{member_name}"
    @clients.each do |c|
      c.puts "JOINED_CHATROOM: #{@name}\nROOM_REF: #{@ref}\nJOIN_ID: #{join_id}"# unless c == member
    end
    while message = member.gets.chomp
      m[0] = message
      m[1] = member.gets.chomp
      m[2] = member.gets.chomp
      m[3] = member.gets.chomp  #capture second \n at the end of string
      if m[3].include?('LEAVE_CHATROOM: ')
        @clients.each do |c|
          c.puts "LEFT_CHATROOM: #{@ref}\nJOIN_ID: #{member_name}"
          puts "Leave message sent to #{c}"
        end
        @clients.delete(member)
        puts "Member deleted: #{member_name}"
        break
      else
        m[4] = member.gets.chomp
        m[4] = m[3].split(' ')
        messaging(member, member_name, m[4][1..m[4].length-1].join(' '))
      end
    end
  end

  def messaging(member, member_name, message)
    puts "Message received from #{member_name}: #{message}"
    @clients.each do |c|
      c.puts "CHAT: #{@ref}\nCLIENT_NAME: #{member_name}\nMESSAGE: #{message}" unless c == member
      puts "Message sent to #{c}" unless c == member
    end
  end

end

# main code starts here
server = TCPServer.open(port)   # Socket to listen on port 8000
pool = ThreadPool.new(max_clients)  # declare pool with max number of clients
room_names = []
chatroom = []
chatroom_number = 0
count = 0

loop {                          # Servers run forever
  Thread.start(server.accept) do |client|
    pool.dispatch(client) do |c|
      loop {
        message = ""
        while (lines = c.gets.chomp) != ""  # Read lines from the socket
          count += 1
          if lines.include?('DISCONNECT: ')
            option = 4
            message = lines
            puts message
            break
          elsif lines == 'KILL_SERVICE'
            option = 3
            message = lines
            puts message
            break
          elsif lines.include?('HELO ')
            message = lines
            puts message
            option = 2
            break
          else
            if count == 4
              message << lines
              option = 1
              break
            else message << lines + "\n"
            end
          end
        end
        count = 0
        if option == 3
          exit
        elsif option == 4
          c.close
        elsif option == 2
          response = "#{message}\nIP:#{Socket.gethostname}\nPort:#{port}\nStudentID:10703297"
          c.puts(response)
        elsif option == 1
            m = message.split("\n") #split string into its 4 lines
            room_name = (m[0].split(' ')[1..m[0].split(' ').length-1]).join(' ')    #extract room name from string
            client_name = (m[3].split(' ')[1..m[3].split(' ').length-1]).join(' ')  #extract client name from string
            if room_names.include?(room_name)
              puts 'Chatroom already exists'
              hash = Hash[room_names.map.with_index.to_a]
              chatroom[hash[room_name]].serve(c, client_name)
            else
              room_names << room_name
              chatroom_number = chatroom_number + 1
              puts 'Chatroom does not already exist'
              chatroom[chatroom_number-1] = Chatroom.new(room_name, room_names.length)
              chatroom[chatroom_number-1].serve(c, client_name)
            end
        end
      }
    end
  end
}