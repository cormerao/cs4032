#Name: Oliver Cormerais
#Student No: 10703297
#CS4032: File Server 1

#!/usr/bin/ruby

require 'socket'                # Get sockets from library
require 'thread'

port = 1000
max_clients = 10

# ThreadPool class
class ThreadPool
  def initialize(max_clients)
    @pool = []
    @max_clients = max_clients
    @pool_semaphore = Mutex.new
    @pool_cv = ConditionVariable.new
  end

  def dispatch(*args)
    Thread.new do
      @pool_semaphore.synchronize do
        while @pool.size >= @max_clients
          print "Pool is full; waiting to run #{args.join(', ')}...\n"
          # wait until some other thread calls @pool_cv.signal
          @pool_cv.wait(@pool_semaphore)
        end
      end
      @pool << Thread.current   # add thread to pool
      print "Current number of active threads: #{@pool.size}\n"
      # exception block starts here
      begin
        yield(*args)
      rescue => e
        exception(self, e, *args)
      ensure
        @pool_semaphore.synchronize do
          @pool.delete(Thread.current)  # delete current thread
          print "Current number of active threads: #{@pool.size}\n\n"
          @pool_cv.signal       # signal to pool that there is a vacancy in the pool
        end
      end #exception block ends here
    end
  end

  def exception(thread, exception, *original_args)
    puts "Exception in thread #{thread}: #{exception}"
  end
end

# main code starts here
server = TCPServer.open(port)   # Socket to listen on port 1000
pool = ThreadPool.new(max_clients)  # declare pool with max number of clients
count = 0
puts "File Server 1 (port: 1000)"
puts "Files in server:"
Dir.foreach('Files1') {|x| puts x if x != "." && x != ".."}

loop {                          # Servers run forever
  Thread.start(server.accept) do |client|
    pool.dispatch(client) do |c|
      replicate = c.gets.chomp
      file_name = c.gets.chomp
      if replicate == 'update'
        read_write = c.gets.chomp
        puts "Sending file #{file_name} to client #{Socket.gethostname}..."
        file = File.open("Files1/#{file_name}", 'rb')
        fileContent = file.read
        fileContent += "\nendFlag"
        c.puts(fileContent)
        file.close
        if(read_write == 'write')
          puts "File sent.\n"
          file_data = ''
          while (file_line = c.gets.chomp) != "endFlag"
            file_data << file_line
            file_data << "\n"
          end
          destFile = File.open("Files1/#{file_name}", 'wb');
          destFile.print file_data
          destFile.close
          puts "File #{file_name} uploaded successfully from #{Socket.gethostname}."

          file_data += "\nendFlag"
          s1 = TCPSocket.open('localhost', 1001)
          s1.puts('replicate')
          s1.puts(file_name)
          s1.puts(file_data)

          s2 = TCPSocket.open('localhost', 1002)
          s2.puts('replicate')
          s2.puts(file_name)
          s2.puts(file_data)
        else
          puts "File sent (read only mode).\n"
        end
      elsif replicate == 'replicate'
        new_file = ''
        while (file_line = c.gets.chomp) != "endFlag"
          new_file << file_line
          new_file << "\n"
        end
        if File.file?("Files1/#{file_name}")
          destFile = File.open("Files1/#{file_name}", 'wb');
          destFile.print new_file
          destFile.close
          puts "File #{file_name} updated successfully from #{Socket.gethostname}."
        end
      end
      c.close
    end
  end
}